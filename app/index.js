import http from 'http';
import ws from 'websocket';
import redis from 'redis';

const APP_ID = process.env.APPID;
const WebSocketServer = ws.server;
const LIVE_CHAT_CHANNEL = 'LIVE_CHAT';
const connections = [];

// Redis restriction: you can't pub-sub using same TCP connection
const subscriber = redis.createClient({
  port: 6379,
  host: 'rds',
});

const publisher = redis.createClient({
  port: 6379,
  host: 'rds',
});

subscriber.on('subscribe', () => {
  console.log(`Server ${APP_ID} successfully subscribed to ${LIVE_CHAT_CHANNEL}`);
  publisher.publish(LIVE_CHAT_CHANNEL, `I'm Server ${APP_ID} and I've subscribed`);
});

subscriber.on('message', (channel, message) => {
  try {
    console.log(`Server ${APP_ID} received a message from ${channel}: ${message}`);
    connections.forEach((c) => c.send(`${APP_ID}: ${message}`));
  } catch (e) {
    console.log('ERROR:', e);
  }
});

subscriber.subscribe(LIVE_CHAT_CHANNEL);

// Raw http server
// Creates TCP connection leveraged by WS server
const httpserver = http.createServer();

// Pass the httpserver instance to WS server
const websocketServer = new WebSocketServer({
  httpServer: httpserver,
});

httpserver.listen(8080, () => console.log('Listening on port 8080...'));

// When WS request comes listen to it and get the connection .. once you get a connection that's it!
websocketServer.on('request', (request) => {
  const connection = request.accept(null, request.origin);

  connection.on('open', () => console.log('Connection: OPEN'));
  connection.on('close', () => console.log('Connection: CLOSE'));

  connection.on('message', (message) => {
    // Publish the message to redis
    const data = message.utf8Data;
    console.log(`App ${APP_ID} received message: ${data}`);
    publisher.publish(LIVE_CHAT_CHANNEL, data);
  });

  setTimeout(() => connection.send(`Connected successfully to server ${APP_ID}`), 5000);
  connections.push(connection);
});

/*
  // Code clean up after closing connection
  subscriber.unsubscribe();
  subscriber.quit();
  publisher.quit();
*/
