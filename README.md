# Runbook

1. Build Docker image
   ```
   yarn run build
   ```
   OR
   ```
   docker build -t wsapp .
   ```
2. Run Docker container
   ```
   yarn start
   ```
   OR
   ```
   docker-compose up
   ```
3. Simulate client via browser console
   ```
   const ws = new WebSocket('ws://localhost:8080');
   ws.onmessage = (message) => console.log(`Received: ${message.data}`);
   ws.send(`Hello! I'm client`);
   ```
4. Open multiple consoles to simulate multiple clients
