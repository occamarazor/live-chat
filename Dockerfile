FROM node:15
WORKDIR /home/node/app
COPY package.json /home/node
COPY app /home/node/app
RUN npm install
CMD npm run app
